package com.rave.simplecalculator

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class CalculatorViewModelTest {

    private val calcViewModel = CalculatorViewModel()

    @Test
    @DisplayName("Test entering first number")
    fun testNum1() {
        // Given

        // When
        calcViewModel.onAction(CalculatorAction.Number(1))

        // Then
        Assertions.assertEquals("1", calcViewModel.state.number1)
    }

    @Test
    @DisplayName("Test clearing calculator")
    fun testClear() {
        // Given

        // When
        calcViewModel.onAction(CalculatorAction.Number(1))
        calcViewModel.onAction(CalculatorAction.Clear)

        // Then
        Assertions.assertEquals("", calcViewModel.state.number1)
    }

    @Test
    @DisplayName("Test entering operator")
    fun testOperator() {
        // Given

        // When
        calcViewModel.onAction(CalculatorAction.Clear)
        calcViewModel.onAction(CalculatorAction.Number(7))
        calcViewModel.onAction(CalculatorAction.Operation(CalculatorOperation.Add))

        // Then
        Assertions.assertEquals("+", calcViewModel.state.operation?.symbol)
    }

    @Test
    @DisplayName("Test entering second number")
    fun testNum2() {
        // Given

        // When
        calcViewModel.onAction(CalculatorAction.Clear)
        calcViewModel.onAction(CalculatorAction.Number(7))
        calcViewModel.onAction(CalculatorAction.Operation(CalculatorOperation.Add))
        calcViewModel.onAction(CalculatorAction.Number(3))

        // Then
        Assertions.assertEquals("3", calcViewModel.state.number2)
    }

    @Test
    @DisplayName("Test performing calculation")
    fun testCalculation() {
        // Given

        // When
        calcViewModel.onAction(CalculatorAction.Clear)
        calcViewModel.onAction(CalculatorAction.Number(7))
        calcViewModel.onAction(CalculatorAction.Operation(CalculatorOperation.Add))
        calcViewModel.onAction(CalculatorAction.Number(3))
        calcViewModel.onAction(CalculatorAction.Calculate)

        // Then
        Assertions.assertEquals("10.0", calcViewModel.state.number1)
    }
}